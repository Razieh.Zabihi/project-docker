FROM node:16.16.0-alpine3.16
RUN addgroup app && adduser -S -G app app
USER app
WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
ENV API_URL=http://example.com/API_URL
EXPOSE 5000
CMD ["npm","start"]
